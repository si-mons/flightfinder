/**
 * ENG phrases.
 */
export enum Phrases {
	'combobox.from' = 'where you at?',
	'combobox.to' = 'where you going?',
	'departs.in' = 'departs in',
}
