import { Airport } from '@flightfinder/entities/Airport';

type AirportSelection = {
	from?: Airport;
	to?: Airport;
};
