import { Flight } from '@flightfinder/entities';
import React from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import '../css/FlightItem.css';
import { CountdownBox } from './countdownBox';

interface FlightItemProps {
	flight: Flight;
	onSelection: () => void;
}

export const FlightItem: React.FC<FlightItemProps> = ({ flight }) => {
	return (
		<Row>
			<Col>
				<Card>
					<Card.Body>
						<Card.Title>
							{flight.nr}, {flight.airline}
						</Card.Title>
						<Card.Subtitle>{flight.aircraft.name}</Card.Subtitle>
						<CountdownBox flight={flight} />
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
};
