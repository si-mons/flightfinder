import React from 'react';
import { Autocomplete } from '@fdefelici/react-bootstrap-combobox';
import { Airport } from '@flightfinder/entities/Airport';
import '../css/AirportCombobox.css';

interface AirportComboboxProps {
	label: string;
	loadAirportsByText(text: string): Promise<Airport[]>;
	onSelection(airport: Airport): void;
	onClear(): void;
}

export const AirportCombobox: React.FC<AirportComboboxProps> = ({
	label,
	loadAirportsByText,
	onSelection,
	onClear,
}) => {
	return (
		<div className='AirportCombobox'>
			<Autocomplete
				labels={{
					'cap.placeholder': label,
					'lst.empty': 'No airports',
				}}
				delay='0.3'
				onChangeAfterCharNum='3'
				searchFun={(
					text: string,
					callback: (dropdownData: object[]) => void
				) => {
					loadAirportsByText(text).then((airports) => {
						// go from airports to dropdown data
						const data: object[] = [];
						airports.forEach((airport) => {
							data.push({
								label: `${airport.iata} - ${airport.name}`,
								value: airport,
								icon: '',
							});
						});
						// use
						callback(data);
					});
				}}
				onSelection={(selection: any) => {
					// execute interface function
					const airport = selection.value as Airport;
					if (airport) {
						onSelection(selection.value as Airport);
					}
				}}
				onClear={() => {
					onClear();
				}}
			/>
		</div>
	);
};
