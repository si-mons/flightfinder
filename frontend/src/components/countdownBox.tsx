import React, { useState } from 'react';
import { Flight } from '@flightfinder/entities';
import { Phrases } from '../phrases/en';
import moment from 'moment';

interface CountdownBoxProps {
	flight: Flight;
}

export const CountdownBox: React.FC<CountdownBoxProps> = ({ flight }) => {
	const time = moment(flight.departTimeUtc).utc(true);
	const [fromNow, setFromNow] = useState(time.fromNow(true));

	// periodically update from now string
	setTimeout(() => {
		setFromNow(time.fromNow(true));
	}, 60000);

	return (
		<span>
			{Phrases['departs.in']} {fromNow}
		</span>
	);
};
