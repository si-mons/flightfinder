import React, { useState } from 'react';
import { Flight } from '@flightfinder/entities';
import Container from 'react-bootstrap/Container';
import { FlightItem } from './flightItem';

interface FlightListProps {
	flights: Flight[];
	onSelection: () => void;
}

export const FlightList: React.FC<FlightListProps> = ({
	flights,
	onSelection,
}) => {
	return (
		<Container fluid className='List'>
			{flights.map((flight) => {
				return (
					<FlightItem
						key={flight.nr + flight.departTimeUtc}
						flight={flight}
						onSelection={onSelection}
					/>
				);
			})}
		</Container>
	);
};
