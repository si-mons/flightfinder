import React, { useState } from 'react';
import '../css/App.css';
import { AirportCombobox } from './airportCombobox';
import { API } from '../api/API';
import { Phrases } from '../phrases/en';
import { Airport, Flight, Aircraft } from '@flightfinder/entities';
import { AirportSelection } from '../types/AirportSelection';
import { FlightList } from './flightList';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// store selection
const selectedAirports: AirportSelection = {};
function App(this: any) {
	// get stateful flight array
	const [currentFlights, setFlights] = useState<Flight[]>([]);
	// listener for selection updates
	const onSelection = (prop: 'from' | 'to', airport: Airport) => {
		// update selection
		selectedAirports[prop] = airport;
		if (selectedAirports.from) {
			// trigger search
			API.flightsByAirport(selectedAirports.from, selectedAirports.to).then(
				(fl) => {
					// set new flights
					setFlights(fl);
				}
			);
		}
	};
	// listener for combo clear
	const onClear = (prop: 'from' | 'to') => {
		selectedAirports[prop] = undefined;
	};

	return (
		<div className='App'>
			<Container>
				<Row xs='1' sm='1' lg='2'>
					<Col>
						<div className='heading'>flightfinder</div>
						<AirportCombobox
							label={Phrases['combobox.from']}
							loadAirportsByText={API.airportsByText}
							onSelection={onSelection.bind(this, 'from')}
							onClear={onClear.bind(this, 'from')}
						></AirportCombobox>
						<AirportCombobox
							label={Phrases['combobox.to']}
							loadAirportsByText={API.airportsByText}
							onSelection={onSelection.bind(this, 'to')}
							onClear={onClear.bind(this, 'to')}
						></AirportCombobox>
					</Col>
					<Col>
						<FlightList flights={currentFlights} onSelection={console.log} />
					</Col>
				</Row>
			</Container>
		</div>
	);
}

export default App;
