import { Airport, Aircraft, Location, Flight } from '@flightfinder/entities';
import moment from 'moment';

export abstract class API {
	public static airportsByText(text: string): Promise<Airport[]> {
		return new Promise((resolve, reject) => {
			fetch(`/api/airports/bystring?q=${text}`)
				.then((res) => res.json())
				.then((data) => {
					// resolve parsed
					resolve(JSON.parse(data) as Airport[]);
				})
				.catch(reject);
		});
	}

	public static flightsByAirport(
		from: Airport,
		to?: Airport
	): Promise<Flight[]> {
		return new Promise((resolve, reject) => {
			fetch(
				`api/flights/byicao?icaoOrigin=
				${from.icao}&icaoDestination=${to ? to.icao : ''}`
			)
				.then((res) => res.json())
				.then((data) => {
					// parse
					resolve(JSON.parse(data) as Flight[]);
				})
				.catch(reject);
		});
	}
}
