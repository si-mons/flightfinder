React frontend for flightfinder app

## Available Scripts

#### In the frontend directory, you can run:

`start:dev` Starts the dev server and the app on port 3000  
`start` Starts the server on port 3000  
`build` Builds the app for production  
`test` Runs the frontend test
