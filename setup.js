const childProcess = require('child_process');

// install main
console.info('installing main project');
childProcess.execSync('npm i');

// get fs extra
const fs = require('fs-extra');

// installing sub projects
console.info('installing sub projects');
childProcess.execSync('npm i', { cwd: './entities' }, console.log);
childProcess.execSync('npm i', { cwd: './backend' }, console.log);
childProcess.execSync('npm i', { cwd: './frontend' }, console.log);

// building sub projects
console.info('building sub projects');
childProcess.execSync('npm run build', { cwd: './entities' }, console.log);
childProcess.execSync('npm run build', { cwd: './backend' }, console.log);
childProcess.execSync('npm run build', { cwd: './frontend' }, console.log);

// copying dist and build folders
console.info('copy to out path');
fs.removeSync('./dist/');
fs.mkdirSync('./dist');
fs.copySync('./frontend/build', './dist/frontend');
