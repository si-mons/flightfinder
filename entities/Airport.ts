import { Location } from './Location';

export class Airport {
	public icao: string;
	public iata: string;
	public name: string;
	public shortname: string;
	public municipalityName: string;
	public location: Location;
	public countryCode: string;

	constructor(
		icao: string,
		iata: string,
		name: string,
		shortname: string,
		municipalityName: string,
		location: Location,
		countryCode: string
	) {
		this.icao = icao;
		this.iata = iata;
		this.name = name;
		this.shortname = shortname;
		this.municipalityName = municipalityName;
		this.location = location;
		this.countryCode = countryCode;
	}
}
