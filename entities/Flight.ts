import { Aircraft } from './Aircraft';

export class Flight {
	public nr: string;
	public fromICAO: string;
	public toICAO: string;
	public aircraft: Aircraft;
	public airline: string;
	public departTimeUtc: string;
	public status: string;

	constructor(
		nr: string,
		fromICAO: string,
		toICAO: string,
		aircraft: Aircraft,
		airline: string,
		departTimeUtc: string,
		status: string
	) {
		this.nr = nr;
		this.fromICAO = fromICAO;
		this.toICAO = toICAO;
		this.aircraft = aircraft;
		this.airline = airline;
		this.departTimeUtc = departTimeUtc;
		this.status = status;
	}
}
