export class Aircraft {
	public name: string;
	public imageUrl: string;

	constructor(name: string, imageUrl?: string) {
		this.name = name;
		this.imageUrl = imageUrl || '';
	}
}
