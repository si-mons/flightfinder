import { Router } from 'express';
import { IFlightFinder } from 'src/interfaces/IFlightFinder';

/**
 * Base route class
 */
export abstract class BaseRoute {
	protected router: Router;
	protected remoteInterface: IFlightFinder;

	constructor(remoteInterface: IFlightFinder) {
		// init router
		this.router = Router();
		this.remoteInterface = remoteInterface;
	}

	public getRouter() {
		return this.router;
	}
}
