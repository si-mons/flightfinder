import { Request, Response, Router } from 'express';
import { BAD_REQUEST, OK, INTERNAL_SERVER_ERROR } from 'http-status-codes';

import { paramMissingError } from '@shared/constants';
import { BaseRoute } from './BaseRoute';
import { IFlightFinder } from 'src/interfaces/IFlightFinder';
import moment from 'moment';

/**
 * Flight route that populates all flight endpoints and connects them to given interface
 */
export class FlightRoute extends BaseRoute {
	constructor(remoteInterface: IFlightFinder) {
		super(remoteInterface);
		this.router.get('/byicao', this.byICAO.bind(this));
		this.router.get('/bynranddate', this.byNrAndDate.bind(this));
	}

	private byNrAndDate(req: Request, res: Response) {
		// get params
		const { nr, date } = req.query;

		if (!nr || !date) {
			// exit with error
			res.status(BAD_REQUEST).json(paramMissingError);

			return;
		}

		// parse date
		const dateObj = moment(date as string).toDate();

		this.remoteInterface
			.flightByNumberAndDate(nr as string, dateObj)
			.then((flight) => {
				// send data
				res.status(OK).json(JSON.stringify(flight));
			})
			.catch((data) => {
				// send data with error code
				res.status(INTERNAL_SERVER_ERROR).json(data);
			});
	}

	private byICAO(req: Request, res: Response) {
		// get params
		const { icaoOrigin, icaoDestination } = req.query;

		if (!icaoOrigin) {
			// exit with error
			res.status(BAD_REQUEST).json(paramMissingError);

			return;
		}

		this.remoteInterface
			.flightsByICAO(icaoOrigin as string, (icaoDestination || '') as string)
			.then((flights) => {
				// send data
				res.status(OK).json(JSON.stringify(flights));
			})
			.catch((data) => {
				// send data with error code
				res.status(INTERNAL_SERVER_ERROR).json(data);
			});
	}
}
