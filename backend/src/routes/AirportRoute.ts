import { Request, Response } from 'express';
import { BAD_REQUEST, OK, INTERNAL_SERVER_ERROR } from 'http-status-codes';

import { paramMissingError } from '@shared/constants';
import { BaseRoute } from './BaseRoute';
import { IFlightFinder } from 'src/interfaces/IFlightFinder';

/**
 * Route that populates airport endpoints and connects them to given interface
 */
export class AirportRoute extends BaseRoute {
	constructor(remoteInterface: IFlightFinder) {
		// super call to add interface
		super(remoteInterface);
		// define routes with bound methods
		this.router.get('/bystring', this.byString.bind(this));
	}

	/**
	 * Finds airports by string using interface
	 * @param req Request
	 * @param res Result
	 */
	private byString(req: Request, res: Response) {
		// get params
		const { q, limit } = req.query;

		if (!q) {
			// exit with error
			res.status(BAD_REQUEST).json(paramMissingError);

			return;
		}

		// get airports
		this.remoteInterface
			.airportsBySting(q as string, parseInt((limit || 10) as string, 10))
			.then((airports) => {
				// send data
				res.status(OK).json(JSON.stringify(airports));
			})
			.catch((data) => {
				// send data with error code
				res.status(INTERNAL_SERVER_ERROR).json(data);
			});
	}
}
