import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import helmet from 'helmet';

import express, { Request, Response, NextFunction, Router } from 'express';
import { BAD_REQUEST, NOT_FOUND } from 'http-status-codes';
import 'express-async-errors';

import logger from '@shared/Logger';
import { AeroDataBoxGateway } from './gateways/aerodatabox/AeroDataBoxGateway';
import { AirportRoute } from '@routes/AirportRoute';
import { FlightRoute } from '@routes/FlightRoute';

// init express
const app = express();

/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

// show routes called in console during development
if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
}

// security
if (process.env.NODE_ENV === 'production') {
	app.use(helmet());
}

// application gateway
const gateway = new AeroDataBoxGateway();

// routes creating seperate router for each route
const airportRoute = new AirportRoute(gateway);
const flightRoute = new FlightRoute(gateway);

// create router
const router = Router();

// use routers connected to gateway
router.use('/airports', airportRoute.getRouter());
router.use('/flights', flightRoute.getRouter());

// add main router to app
app.use('/api', router);

// print API errors
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
	logger.error(err.message, err);
	return res.status(BAD_REQUEST).json({
		error: err.message,
	});
});

/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/

app.get('*', (req: Request, res: Response) => {
	res.status(NOT_FOUND).end();
});

// export express instance
export default app;
