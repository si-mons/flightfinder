/**
 * Supported methods.
 */
export enum Method {
	GET = 'GET',
	POST = 'POST',
}
