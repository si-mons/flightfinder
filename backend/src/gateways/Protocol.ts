/**
 * Supported protocols.
 */
export enum Protocol {
	HTTP = 'http:',
	HTTPS = 'https:',
}
