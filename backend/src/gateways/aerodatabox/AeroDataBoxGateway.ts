import { Protocol } from '../Protocol';
import { Method } from '../Method';
import { RemoteGateway } from '../RemoteGateway';
import { IFlightFinder } from 'src/interfaces/IFlightFinder';
import { Aircraft, Airport, Location, Flight } from '@flightfinder/entities';
import moment from 'moment';

/**
 * Gateway to connect to are data box api
 */
export class AeroDataBoxGateway extends RemoteGateway implements IFlightFinder {
	private static HOST = 'aerodatabox.p.rapidapi.com';
	private static HEADERS = {
		'x-rapidapi-host': 'aerodatabox.p.rapidapi.com',
		// various api keys as contingent ran out
		// 'x-rapidapi-key': '9e7026e0f0msh9f23567f76fa7e2p15aaf0jsn377b5f05c372',
		// 'x-rapidapi-key': 'd47fb865c4msh238bfa6dd0e0eddp104a36jsna271fa3b7853',
		'x-rapidapi-key': '6af89c6012mshb22cc917378307bp12fdb3jsnbf4584fad58a',
	};
	private static DATETIME_FORMAT = 'YYYY-MM-DDTHH:mm';
	private static DATE_FORMAT = 'YYYY-MM-DD';

	constructor() {
		super(AeroDataBoxGateway.HOST, undefined, Protocol.HTTPS);
	}

	public flightByNumberAndDate(nr: string, date: Date): Promise<Flight> {
		// return promise
		return new Promise(async (resolve, reject) => {
			// format date
			const formattedDate = moment(date).format(AeroDataBoxGateway.DATE_FORMAT);
			// define path
			const path = `/flights/${nr}/${formattedDate}`;

			// request data
			this.request(
				path,
				{
					headers: AeroDataBoxGateway.HEADERS,
					qs: { withLocation: false, withAircraftImage: true },
				},
				Method.GET
			)
				.then((data) => {
					// init icao codes
					let icaoOrigin = '';
					let icaoDestination = '';
					// parse result and create instances
					const json = (JSON.parse(data) as [{}]) || null;
					const item = json ? (json[0] as any) : '';
					if (item) {
						// get origin icao
						icaoOrigin =
							item.departure && item.departure.airport
								? item.departure.airport.icao
								: '';
						// get destination icao
						icaoDestination =
							item.arrival && item.arrival.airport
								? item.arrival.airport.icao
								: '';
					}
					// resolve with flight
					resolve(this.flightFromObject(icaoOrigin, icaoDestination, item));
				})
				.catch(reject);
		});
	}

	public localTimeByICAO(icao: string): Promise<Date> {
		// return promise
		return new Promise(async (resolve, reject) => {
			// define path
			const path = `/airports/icao/${icao}/time/local`;

			// request data
			this.request(
				path,
				{
					headers: AeroDataBoxGateway.HEADERS,
					qs: { withLeg: true, direction: 'Departure' },
				},
				Method.GET
			)
				.then((data) => {
					// parse result and create instances
					const json = JSON.parse(data) || null;
					const dateString = json ? (json.utcTime as string) : '';
					// create date
					const date = moment(
						dateString,
						AeroDataBoxGateway.DATETIME_FORMAT
					).toDate();
					// resolve with date
					resolve(date);
				})
				.catch(reject);
		});
	}

	public airportsBySting(q: string, limit = 10): Promise<Airport[]> {
		// return promise
		return new Promise(async (resolve, reject) => {
			// request data
			this.request(
				'/airports/search/term',
				{
					headers: AeroDataBoxGateway.HEADERS,
					qs: { q, limit },
				},
				Method.GET
			)
				.then((data) => {
					// init result array
					const airports: Airport[] = [];
					// parse result and create instances
					const json = JSON.parse(data) || null;
					const items = json ? (json.items as [{}]) : null;
					if (items && typeof items.forEach === 'function') {
						items.forEach((item: any) => {
							airports.push(this.airportFromObject(item));
						});
					}
					// resolve with data
					resolve(airports);
				})
				.catch(reject);
		});
	}

	public flightsByICAO(from: string, to?: string): Promise<Flight[]> {
		// return promise
		return new Promise(async (resolve, reject) => {
			// get local airport time
			const localAirportTime = moment(await this.localTimeByICAO(from));
			// to utc time
			const time = localAirportTime.add(localAirportTime.utcOffset(), 'm');
			// from and till strings
			const fromStr = time.format(AeroDataBoxGateway.DATETIME_FORMAT);
			const tillStr = moment(localAirportTime)
				.add(12, 'h')
				.format(AeroDataBoxGateway.DATETIME_FORMAT);

			// define path
			const path = `/flights/airports/icao/${from}/${fromStr}/${tillStr}`;
			// request data
			this.request(
				path,
				{
					headers: AeroDataBoxGateway.HEADERS,
					qs: { withLeg: true, direction: 'Departure' },
				},
				Method.GET
			)
				.then((data) => {
					// init result array
					const flights: Flight[] = [];
					// parse result and create instances
					const json = JSON.parse(data) || null;
					const items = json ? (json.departures as [{}]) : null;
					if (items && typeof items.forEach === 'function') {
						items.forEach((item: any) => {
							// get destination icao
							const icaoDestination =
								item.arrival && item.arrival.airport
									? item.arrival.airport.icao
									: '';
							// check if need filter
							if (to && icaoDestination !== to) {
								// continue as requestet to icao differs
								return;
							}
							// swap order of items
							flights.push(this.flightFromObject(from, icaoDestination, item));
						});
					}
					// resolve flights sorted by date
					resolve(
						flights.sort((a, b) => {
							if (
								moment(a.departTimeUtc).toDate() <
								moment(b.departTimeUtc).toDate()
							) {
								return -1;
							}
							return 1;
						})
					);
				})
				.catch(reject);
		});
	}

	private airportFromObject(obj: any): Airport {
		return new Airport(
			obj.icao,
			obj.iata,
			obj.name,
			obj.shortname,
			obj.municipalityName,
			new Location(
				obj.location ? obj.location.lat : 0,
				obj.location ? obj.location.lon : 0
			),
			obj.countryCode
		);
	}

	private flightFromObject(
		icaoOrigin: string,
		icaoDestination: string,
		obj: any
	): Flight {
		return new Flight(
			obj.number,
			icaoOrigin,
			icaoDestination,
			new Aircraft(
				obj.aircraft ? obj.aircraft.model : '',
				obj.aircraft
					? obj.aircraft.image
						? obj.aircraft.image.url
						: null
					: null
			),
			obj.airline ? obj.airline.name : '',
			obj.departure
				? moment(
						obj.departure.scheduledTimeUtc,
						AeroDataBoxGateway.DATETIME_FORMAT
				  ).format()
				: '',
			obj.status
		);
	}
}
