import * as request from 'request';
import URI from 'urijs';
import { Protocol } from './Protocol';
import { Method } from './Method';

/**
 * Base gateway used to connect to any remote api
 */
export abstract class RemoteGateway {
	protected protocol: Protocol;
	protected host: string;
	protected port: string;

	constructor(host: string, port?: string, protocol = Protocol.HTTP) {
		this.protocol = protocol;
		this.host = host;
		this.port = port || '';
	}

	protected request(
		path: string,
		options?: request.CoreOptions,
		method = Method.GET
	): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			// get request function depending on set method
			const requester = method === Method.GET ? request.get : request.post;

			// build uri
			const uri = new URI()
				.host(this.host)
				.path(path)
				.port(this.port.toString())
				.protocol(this.protocol);

			// do request
			requester(
				uri.valueOf(),
				options,
				(error: any, response: request.Response, body: any) => {
					// check for error
					if (error) {
						reject(error);
						return;
					}

					resolve(body);
				}
			);
		});
	}
}
