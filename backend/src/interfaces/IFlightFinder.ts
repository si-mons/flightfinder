import { Airport, Flight } from '@flightfinder/entities';

/**
 * Interface to satisfy if one instance is to be used data provider
 */
export interface IFlightFinder {
	airportsBySting(q: string, limit?: number): Promise<Airport[]>;
	flightsByICAO(from: string, to?: string): Promise<Flight[]>;
	flightByNumberAndDate(nr: string, date: Date): Promise<Flight>;
}
