Express/TS backend for flightfinder app.

## Available Scripts

In the backend directory, you can run:  
`start:dev` Starts the dev server and the app on port 3005  
`start` Starts the server on port 8081  
`build` Builds the app for production  
`test` Runs the backend test

## API

The app populates the following endpoints

#### /api/airports/bystring?q=<string>&limit?=<number>

Returns array of `Airport` by freetext and optional limit

#### /api/flights/byicao?icaoOrigin=<string>&icaoDestination?=<string>

Returns array of `Flight` by origin ICAO code and optional destination ICAO code.
Searches from now until 12h into future.

#### /api/flights/bynranddate?nr=<string>&date=<string>

Returns `Flight` by number and date string. Date string must be in ISO-8601 format.
