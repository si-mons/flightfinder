# README

A mobile optimized app written in typescript and react to find your flight with ease.

The app should enable one user to easily and fast find its next flight. The app is specialized on a quick status check of flights happening within the next 12h.

## Available Scripts

In the project directory, you can run:  
`npm run setup` Installs and builds all subprojects  
`npm run start:dev` Starts front and backend in dev mode  
`npm run start:back:dev` Starts backend in dev mode  
`npm run start:front:dev` Starts frontend in dev mode

### Get going quickly

- Clone
- Run `npm run setup`
- Run `npm start:dev`

## Further information

The app consists of a node-backend using express wich abstracts and simplyfies virtually any Flight-Data-API as long as it satisfies the IFlightFinder interface.
Then the backend populates minimalistic endpoints for the frontend (more information under Backend).
Also, the app serves a react one-page app wich connects to the API populated by the backend and enables one user to enter where he currently is (eg airport/city name) and where to go. Then the app finds both for from- and to-input airport-suggestions.
Once at least the from-airport is selected, the app finds flights that go off that airports in the near future and shows them to the user.

## Architecture

All across the application typescript is used. There is also a thrid module (/entities) wich exports classes used by both front and back to make JSON parsing easy and safe.

Architectually the backend basically binds a populated endpoint (express) directly to a interface method via route-classes. This makes it easy to implement a new Gateway to consume a different remote api without having to reimplement the actual request-method and type-safe as the gateway provided to the routes have to implement the IFlightFinder interface.

The frontend is held as simple as possible including a abstract class `API` wich provides connection to the endpoints as those should not change even if the remove api does.

## What can be improved

Due to limited time the following has potential to be improved

- Extract aerodatabox gateway as seperate project/module
  - The idea here would be to install whatever gateways one would need. Also the apikey/secret information should be provided during the build process and not live within the source code
- Deploy
  - the app is not yet ready to be deployed in production mode
- Host each module (front/back/entities) in seperate git repos
- Implement detail page for flights (the abstracted endpoint as well as the frontend API method already exists)
- Tests
  - Implement unit tests for backend including a "fake gateway" that eg reads data from json
  - Implement frontend test for the react app using a "fake API"

### Frontend

Check [this](frontend/) for more information

### Backend

Check [this](backend/) for more information
